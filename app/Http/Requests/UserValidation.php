<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Validator;

class UserValidation extends FormRequest
{
  /**
   * Determine if the user is authorized to make this request.
   *
   * @return bool
   */
  public function authorize()
  {
    return true;
  }

  /**
   * Get the validation rules that apply to the request.
   *
   * @return array
   */
  public function rules()
  {
    $user_id = $this->route('user');

    return [
      'name' => 'required|string|max:255',
      'email' => 'required|string|email|unique:users,email,'.$user_id,
      'cpf' => 'required|cpf|unique:users,cpf,'.$user_id,
      'birthday' => 'required|data',
      'password' => 'sometimes|required|min:6|confirmed',
    ];
  }
}
