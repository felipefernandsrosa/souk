<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Foundation\Auth\RegistersUsers;
use App\User;
use App\Http\Requests\UserValidation;
use GuzzleHttp\Client;

class UserController extends Controller
{

  public function __construct() {
    $this->user = new User();
  }

  /**
   * Display a listing of the resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function index(Request $request)
  {
    if($request->has('search') ) {
      $users = $this->user->search($request->search);
    }else{
      $users = $this->user->paginate(5);
    }
    return view('user.index', ['users' => $users]);
  }

  /**
   * Show the form for creating a new resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function create()
  {
    return view('user.create');
  }

  /**
   * Store a newly created resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @return \Illuminate\Http\Response
   */
  public function store(UserValidation $request)
  {
    $request->offsetSet('is_active', 1);

    $user = $this->user->create($request->all());

    return redirect('users');
  }

  /**
   * Display the specified resource.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function show($id)
  {
    header("Content-Type: image/png");
    $getImage = $this->getImageFromAPI($id);
    $type = pathinfo($getImage->data->image_path, PATHINFO_EXTENSION);
    $data = file_get_contents($getImage->data->image_path);
    echo $data;
  }

  /**
   * Show the form for editing the specified resource.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function edit($id)
  {
    $getImage = $this->getImageFromAPI($id);

    if(isset($getImage->errors)) {
      $image = '';
    }else {
      $image = $getImage->data->image_path;
    }

    return view('user.edit', ['user' => $this->user->findOrFail($id)], ['image_path' => $image]);
  }

  /**
   * Update the specified resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function update(UserValidation $request, $id)
  {
    $this->user->fill($request->all());
    $this->user->find($id)->update($request->all());

    return redirect()->route('users.index');
  }

  /**
   * Remove the specified resource from storage.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function destroy($id)
  {
    $this->user->find($id)->delete();

    return redirect()->route('users.index');
  }

  public function getImageFromAPI($id) {
    $client = new Client();
    $res = $client->request('GET', env('SOUK_API').'images/'.$id, ["http_errors" => false]);

    $res = json_decode($res->getBody()->getContents());

    return $res;
  }
}
