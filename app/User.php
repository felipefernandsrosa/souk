<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
  use Notifiable;

  /**
   * The attributes that are mass assignable.
   *
   * @var array
   */
  protected $fillable = ['name', 'email', 'birthday', 'cpf', 'is_active', 'password'];

  /**
   * The attributes that should be hidden for arrays.
   *
   * @var array
   */
  protected $hidden = [
    'password', 'remember_token'
  ];

  public function setPasswordAttribute($password)
  {
    if (!empty($password))
    {
      $this->attributes['password'] = bcrypt($password);
    }
  }

  /**
   * Search on fields: name, email, cpf
   * @param $search
   * @return user object with query params
   **/

  public function search($search) {
    return User::where('name','like', '%'.$search.'%')
      ->orWhere('email', 'like', '%'.$search.'%')
      ->orWhere('cpf', 'like', '%'.$search.'%')
      ->paginate(5)
      ->appends('search', $search);
  }
}
