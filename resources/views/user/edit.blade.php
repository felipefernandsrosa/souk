@extends('layouts.app')

@section('content')
<div class="container">
  <div class="row">
    <h1>Editar usuário</h1>

    <form id="imageForm" class="" method="POST" action="{{env('SOUK_API')}}images/{{$user->id}}">
      {{ csrf_field() }}
      {{ method_field('PUT')}}
      <div class="col-sm-6">
        <img id="photo" src="{{$image_path}}" width="80" />
        <div class="form-group">
          <label for="file" class="control-label">Image</label>
          <input id="file" type="file" class="form-control" name="file" autofocus />
          <input type="submit" value="Enviar" class="btn btn-md btn-primary send" />
        </div>
      </div>
    </form>

    <form class="" method="POST" action="{{ route('users.update', $user->id) }}">
      {{ csrf_field() }}

      {{ method_field('PUT')}}

      @include('layouts.errors.error')

      <div class="col-sm-12">
        <div class="form-group">
          <div class="checkbox">
            <label>
              <input name="is_active" type="checkbox" {{ ($user->is_active == true) ? 'checked' : '' }} value="{{$user->is_active}}"> Ativo
            </label>
          </div>
        </div>
      </div>

      <div class="col-sm-6">
        <div class="form-group">
          <label for="name" class="control-label">Nome</label>
          <input id="name" type="text" class="form-control" name="name" value="{{$user->name}}" autofocus />
        </div>
      </div>

      <div class="col-sm-6">
        <div class="form-group">
          <label for="email" class="control-label">E-mail</label>
          <input id="email" type="text" class="form-control" name="email" value="{{$user->email}}" autofocus />
        </div>
      </div>

      <div class="col-sm-6">
        <div class="form-group">
          <label for="birthday" class="control-label">Aniversário</label>
          <input id="birthday" type="text" class="form-control" name="birthday" value="{{$user->birthday}}" autofocus />
        </div>
      </div>

      <div class="col-sm-6">
        <div class="form-group">
          <label for="cpf" class="control-label">CPF</label>
          <input id="cpf" type="text" class="form-control" name="cpf" value="{{$user->cpf}}" autofocus />
        </div>
      </div>

      <div class="col-sm-6">
        <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
          <label for="password" class="control-label">Senha*</label>
          <input id="password" type="password" class="form-control" name="password">

          @if ($errors->has('password'))
            <span class="help-block">
              <strong>{{ $errors->first('password') }}</strong>
            </span>
          @endif
        </div>
      </div>

      <div class="col-sm-6">
        <div class="form-group">
          <label for="password-confirm" class="control-label">Confirmar Senha</label>
          <input id="password-confirm" type="password" class="form-control" name="password_confirmation">
        </div>
      </div>

      <div class="col-sm-4 pull-right text-right" style="margin-top: 40px;">
        <button type="submit" class="col-xs-12 btn btn-md btn-primary">Enviar</button>
      </div>
    </form>

  </div>
</div>
@endsection
