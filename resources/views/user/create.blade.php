@extends('layouts.app')

@section('content')
<div class="container">
  <div class="row">
    <h1>Adicionar usuário</h1>

    <form class="" method="POST" action="{{ route('users.store') }}">
      {{ csrf_field() }}

      @include('layouts.errors.error')

      <div class="col-sm-6">
        <div class="form-group">
          <label for="name" class="control-label">Nome</label>
          <input id="name" type="text" class="form-control" name="name" value="{{old('name')}}" autofocus />
        </div>
      </div>

      <div class="col-sm-6">
        <div class="form-group">
          <label for="email" class="control-label">E-mail</label>
          <input id="email" type="text" class="form-control" name="email" value="{{old('email')}}" autofocus />
        </div>
      </div>

      <div class="col-sm-6">
        <div class="form-group">
          <label for="birthday" class="control-label">Aniversário</label>
          <input id="birthday" type="text" class="form-control" name="birthday" value="{{old('birthday')}}" autofocus />
        </div>
      </div>

      <div class="col-sm-6">
        <div class="form-group">
          <label for="cpf" class="control-label">CPF</label>
          <input id="cpf" type="text" class="form-control" name="cpf" value="{{old('cpf')}}" autofocus />
        </div>
      </div>

      <div class="col-sm-6">
        <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
          <label for="password" class="control-label">Senha*</label>
          <input id="password" type="password" class="form-control" name="password" required>

          @if ($errors->has('password'))
            <span class="help-block">
              <strong>{{ $errors->first('password') }}</strong>
            </span>
          @endif
        </div>
      </div>

      <div class="col-sm-6">
        <div class="form-group">
          <label for="password-confirm" class="control-label">Confirmar Senha</label>
          <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required>
        </div>
      </div>

      <div class="col-sm-4 pull-right text-right" style="margin-top: 40px;">
        <button type="submit" class="col-xs-12 btn btn-md btn-primary">Enviar</button>
      </div>
    </form>

  </div>
</div>
@endsection
