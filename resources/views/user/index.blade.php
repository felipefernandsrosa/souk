@extends('layouts.app')

@section('content')
<div class="container">
  <div class="row">
    <div class="col-xs-12">

      <h1>Usuários</h1>
      <div class="row" style="margin-bottom: 15px;">
        <div class="col-lg-6">

          <form class="form" method="get" action="{{route('users.index')}}">
            <div class="input-group">
              <input name="search" type="text" class="form-control" placeholder="Search for...">
              <span class="input-group-btn">
                <button class="btn btn-default" type="submit">Buscar</button>
              </span>
            </div>
          </form>

        </div>
      </div>

      <table class="table table-striped table-bordered">
        <thead>
          <tr>
            <td>&nbsp;</td>
            <td>Name</td>
            <td>Email</td>
            <td>Data de Nascimento</td>
            <td>CPF</td>
            <td>Ações</td>
          </tr>
        </thead>
        <tbody>
          @foreach($users as $key => $value)
          <tr>
            <td><img src="{{route('users.show', $value->id)}}" width="50" /></td>
            <td>{{ $value->name }}</td>
            <td>{{ $value->email }}</td>
            <td>{{ $value->birthday }}</td>
            <td>{{ $value->cpf }}</td>
            <td>
              <form action="{{ route('users.destroy', $value->id) }}" method="post">
                {{ csrf_field() }}
                {{ method_field('DELETE')}}
                <a href="{{route('users.edit', $value->id)}}" class="btn btn-warning">Editar</a>
                <button class="btn btn-danger" type="submit">Delete</button>
              </form>
            </td>
          </tr>
          @endforeach
        </tbody>
        <tfoot>
          <td colspan="6">
            <a href="{{route('users.create')}}" class="btn btn-sm btn-success col-xs-12">
              <i class="glyphicon glyphicon-plus"></i>
            </a>
          </td>
        </tfoot>
      </table>

      {{ $users->links() }}

    </div>
  </div>
</div>
@endsection
