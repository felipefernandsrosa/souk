<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <!-- CSRF Token -->
  <meta name="csrf-token" content="{{ csrf_token() }}">
  <title>SOUK</title>

  <!-- Styles -->
  <link href="{{ mix('css/app.css') }}" rel="stylesheet">

</head>
<body>
  <nav class="navbar navbar-default navbar-fixed-top">
    <div class="container">
      <div class="navbar-header"> <button type="button" class="collapsed navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-6" aria-expanded="false"> <span class="sr-only">Toggle navigation</span> <span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span> </button>      <a href="#" class="navbar-brand">Brand</a> </div>
      <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-6">
        <ul class="nav navbar-nav">
          <li class="active"><a href="<?php echo url('/'); ?>">Home</a></li>
          <li><a href="<?php echo url('users/'); ?>">Usuários</a></li>
        </ul>
      </div>
    </div>
  </nav>
  {{-- @include('shared.menu') --}}
  <div id="app">
    @yield('content')
  </div>

  <script type="text/javascript" src="{{ mix('js/app.js') }}"></script>
  <script type="text/javascript">
    jQuery(document).ready(function ($){
      $("#imageForm").submit(function(e){
        e.preventDefault();
        var formData = new FormData(this);

        var url = $(this).attr('action');

        $.ajax({
            url: url,
            type: 'POST',
            data:new FormData($("#imageForm")[0]),
            success: function (data) {
              console.log();
              console.log($('#photo'));
              $('#photo').attr('src', data.data['image_path']);
            },
            cache: false,
            contentType: false,
            processData: false,
        });
      });
    });
  </script>
</body>
</html>
